import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {ContactListComponent} from './components/contact-list/contact-list.component';
import {AppRoutingModule} from "./app.routing.module";
import {BreadcrumbComponent} from './components/breadcrumb/breadcrumb.component';
import {HttpClientModule} from "@angular/common/http";
import {ModalDialogModule} from "ngx-modal-dialog";
import {CustomModalComponent} from './components/custom-modal/custom-modal.component';
import {ContactEditComponent} from './components/contact-edit/contact-edit.component';
import {ReactiveFormsModule} from "@angular/forms";
import {NgxMaskModule} from "ngx-mask";

import localEsDo from '@angular/common/locales/es-DO';
import {registerLocaleData} from "@angular/common";

registerLocaleData(localEsDo);

@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    BreadcrumbComponent,
    CustomModalComponent,
    ContactEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ModalDialogModule.forRoot(),
    NgxMaskModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-DO'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
