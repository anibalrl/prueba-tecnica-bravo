import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ContactsService} from "../../services/contacts.service";
import {MaritalService} from "../../services/marital.service";
import {MaritalStatus} from "../../models/marital-status";
import {ActivatedRoute, Router} from "@angular/router";
import {Contact} from "../../models/contact";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {
  editionForm: FormGroup;
  maritalStatus: Array<MaritalStatus> = [];
  datePipe = new DatePipe('es-DO');
  private readonly contactId: number;

  constructor(private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private router: Router,
              private maritalService: MaritalService,
              private contactsService: ContactsService) {
    this.contactId = activeRoute.snapshot.params.id as number;
    this.loadMaritalStatus();
    this.editionForm = formBuilder.group({
      firstName: ['', [Validators.required, Validators.maxLength(64)]],
      lastName: ['', [Validators.required, Validators.maxLength(64)]],
      phone: ['', [Validators.required, Validators.maxLength(10)]],
      email: ['', [Validators.required, this.email]],
      birthDate: ['', [Validators.required]],
      maritalStatus: [null, [Validators.required]],
    });
    if (this.contactId) {
      this.loadContactInfo();
    }
  }

  ngOnInit(): void {
  }

  loadContactInfo() {
    this.contactsService.getOne(this.contactId)
      .subscribe((contact: Contact) => {
        this.editionForm.patchValue({
          ...contact,
          birthDate: this.datePipe.transform(contact.birthDate, 'yyyy-MM-dd'),
          maritalStatus: contact.maritalStatus.id
        });
        this.editionForm.get('')
      });
  }

  loadMaritalStatus() {
    this.maritalService.getAll()
      .subscribe((statuses: Array<MaritalStatus>) => {
        this.maritalStatus = statuses;
      });
  }

  isInvalid(field: string) {
    return this.editionForm.get(field)?.invalid && !this.editionForm.get(field)?.pristine;
  }

  email(control: FormControl) {
    if (control.value) {
      const matches = control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/);
      return matches ? null : {'invalidEmail': true};
    } else {
      return null;
    }
  }

  saveForm($event: any) {
    $event.preventDefault();
    const form = this.editionForm.getRawValue();
    form.maritalStatus = this.maritalStatus.find(p => p.id == form.maritalStatus);
    form.birthDate = getDate(form.birthDate);
    if (this.contactId) {
      this.contactsService.update(this.contactId, form)
        .subscribe(() => {
          this.router.navigate(['/']);
        }, ()=>{
          alert("Algo salio mal, favor revisar los campos e intentar nuevamente");
        });
    } else {
      this.contactsService.create(form)
        .subscribe(() => {
          this.router.navigate(['/']);
        }, ()=>{
          alert("Algo salio mal, favor revisar los campos e intentar nuevamente");
        });
    }
  }

}

const getDate = (birthDate: string) => {
  const date = new Date(birthDate);
  date.setHours(date.getHours() + 4);
  return date;
}
