import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ContactsService} from "../../services/contacts.service";
import {Contact} from "../../models/contact";
import {ModalDialogService} from "ngx-modal-dialog";
import {CustomModalComponent} from "../custom-modal/custom-modal.component";

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  contacts: Array<Contact> = [];

  constructor(private contactsService: ContactsService,
              private modalService: ModalDialogService,
              private containerRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    this.loadContacts();
  }

  private loadContacts() {
    this.contactsService.getAll()
      .subscribe((response) => {
        this.contacts = response;
      });
  }

  delete(id: number) {
    this.modalService.openDialog(this.containerRef, {
      title: 'Eliminar contacto',
      actionButtons: [
        {
          text: 'Borrar',
          onAction: () => {
            this.doDelete(id);
            return true;
          },
          buttonClass: 'btn btn-danger'
        }
      ],
      childComponent: CustomModalComponent,
      data: {
        onDelete: () => {
          this.doDelete(id);
        }
      }
    });
  }

  private doDelete(id: number) {
    this.contactsService.delete(id)
      .subscribe(() => {
        this.loadContacts();
      });
  }
}
