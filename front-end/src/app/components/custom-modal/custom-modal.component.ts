import {Component, ComponentRef} from '@angular/core';
import {IModalDialog, IModalDialogButton, IModalDialogOptions} from "ngx-modal-dialog";

@Component({
  selector: 'app-custom-modal',
  templateUrl: './custom-modal.component.html',
  styleUrls: ['./custom-modal.component.css']
})
export class CustomModalComponent implements IModalDialog {
  actionButtons: IModalDialogButton[];

  constructor() {
    this.actionButtons = [
      {text: 'Cerrar'},
    ];
  }

  dialogInit(reference: ComponentRef<IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
    if (options.data.onDelete) {
      this.actionButtons.push({
        text: 'Eliminar', buttonClass: 'btn btn-danger', onAction: () => {
          options.data.onDelete();
          return true;
        }
      });
    }
  }
}
