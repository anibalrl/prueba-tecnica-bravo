import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Contact} from "../models/contact";
import {MaritalStatus} from "../models/marital-status";

@Injectable({
  providedIn: 'root'
})
export class MaritalService {
  private readonly baseUrl = '/api/v1/marital-status'

  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<Array<MaritalStatus>>(this.baseUrl);
  }
}
