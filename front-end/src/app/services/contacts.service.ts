import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Contact} from "../models/contact";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  private readonly baseUrl = '/api/v1/contacts'

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<Array<Contact>> {
    return this.http.get<Array<Contact>>(this.baseUrl);
  }

  getOne(contactId: number): Observable<Contact> {
    return this.http.get<Contact>(`${this.baseUrl}/${contactId}`);
  }

  update(contactId: number, contact: Contact): Observable<Contact> {
    return this.http.put<Contact>(`${this.baseUrl}/${contactId}`, contact);
  }

  create(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(`${this.baseUrl}`, contact);
  }

  delete(contactId: number) {
    return this.http.delete<Contact>(`${this.baseUrl}/${contactId}`);
  }
}
