import {MaritalStatus} from "./marital-status";

export interface Contact {
  id: number;
  firstName: string;
  lastName: string;
  phone: string;
  birthDate: Date;
  email: string;
  maritalStatus: MaritalStatus;
}
