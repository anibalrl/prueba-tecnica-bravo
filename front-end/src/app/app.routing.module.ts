import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactListComponent} from "./components/contact-list/contact-list.component";
import {ContactEditComponent} from "./components/contact-edit/contact-edit.component";

const routes: Routes = [
  {
    path: '',
    component: ContactListComponent,
    data: {
      title: 'Lista de contactos',
      includeAdd: true
    }
  },
  {
    path: 'contacts/:id/details',
    component: ContactEditComponent,
    data: {
      title: 'Edición de contacto',
      includeAdd: false
    }
  },
  {
    path: 'contacts/create',
    component: ContactEditComponent,
    data: {
      title: 'Creación de contacto',
      includeAdd: false
    }
  },
  {
    path: '**',
    redirectTo: '/'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {enableTracing: false})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
