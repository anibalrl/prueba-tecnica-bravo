import {Component} from '@angular/core';
import {ActivatedRoute, Data, NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  data: Data = {};
  constructor(private router: Router,
              private route: ActivatedRoute) {
    router.events
      .pipe(filter(r => {
        return r instanceof NavigationEnd;
      }))
      .subscribe(() => {
        this.data = this.lastChild.snapshot.data;
      })
  }

  get lastChild() {
    let lastChild = this.route;
    while (lastChild.firstChild) {
      if (lastChild.firstChild) {
        lastChild = lastChild.firstChild;
      }
    }
    return lastChild;
  }
}
