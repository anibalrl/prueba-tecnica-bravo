package com.bravo.pruebatecnica.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorForwardConfig {

    /**
     * @apiNote This resource is only for not found page management
     */
    @GetMapping(value = "/**/{path:[^.]*}")
    public String forward() {
        return "forward:/";
    }
}