package com.bravo.pruebatecnica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaTecnicaBravoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaTecnicaBravoApplication.class, args);
    }

}
