package com.bravo.pruebatecnica.exceptions.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The contact you're trying to persist already exists.")
public class ContactAlreadyExistsException extends RestResponseException {

    public ContactAlreadyExistsException() {
        super("Already exists", HttpStatus.CONFLICT);
    }

    public ContactAlreadyExistsException(String message) {
        super(message, HttpStatus.CONFLICT);
    }
}

