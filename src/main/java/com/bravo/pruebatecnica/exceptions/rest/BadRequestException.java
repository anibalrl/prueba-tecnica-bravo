package com.bravo.pruebatecnica.exceptions.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "There's an invalid item on your request.")
public class BadRequestException extends RestResponseException {

    public BadRequestException() {
        super("Bad Request.", HttpStatus.BAD_REQUEST);
    }

    public BadRequestException(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }
}
