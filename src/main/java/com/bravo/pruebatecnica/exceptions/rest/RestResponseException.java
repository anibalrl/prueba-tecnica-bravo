package com.bravo.pruebatecnica.exceptions.rest;


import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientResponseException;

public class RestResponseException extends RestClientResponseException {

    private final HttpStatus status;

    RestResponseException(String message, HttpStatus status) {
        super(message, status.value(), message, null, null, null);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return this.status;
    }
}
