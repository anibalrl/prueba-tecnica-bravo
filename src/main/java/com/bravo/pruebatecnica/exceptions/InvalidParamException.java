package com.bravo.pruebatecnica.exceptions;

public class InvalidParamException extends RuntimeException {
    public InvalidParamException(String param) {
        super(String.format("El parámetro (%s) debe ser mayor que cero.", param));
    }
}
