package com.bravo.pruebatecnica.algorithms;


import com.bravo.pruebatecnica.exceptions.InvalidParamException;

public class Numbers {

    /**
     * @param a primer parámetro de la concatenación, se representará al principio del resultado
     * @param b segundo parámetro de la concatenación, se representará al final del resultado
     * @return entero con el resultado de la concatenación
     */
    public static int concatenate(int a, int b) {
        if (a < 0) {
            throw new InvalidParamException("a");
        } else if (b < 0) {
            throw new InvalidParamException("b");
        } else {
            int pow = 10;
            while (b >= pow) {
                pow *= 10;
            }
            return a * pow + b;
        }
    }

    /**
     * @param number El número que se quiere determinar si es par.
     * @return booleano indicando si el número es o no par.
     */
    public static boolean isEven(int number) {
        return ((number & 0x1) == 0);
    }

    /**
     * @param multiplying El número que se desea multiplicar
     * @param multiplier  La cantidad de veces que se desea multiplicar el número @multiplying
     * @return el resultado de la multiplicación mediante suma sucesivas
     */
    public static int multiply(int multiplying, int multiplier) {
        if (multiplier == 0) {
            return 0;
        } else if (multiplying == 0) {
            return 0;
        } else if (multiplying < 0) {
            throw new InvalidParamException("multiplying");
        } else if (multiplier < 0) {
            throw new InvalidParamException("multiplier");
        }
        return multiplying + multiply(multiplying, multiplier - 1);
    }

    /**
     * @param number el numero que se desea reversar
     * @return el numero de entrada invertido
     */
    public static int reverse(int number) {
        if (number == 0) {
            return 0;
        }
        return (number % 10) * ((int) Math.pow(10, ((int) Math.log10(number)))) + reverse(number / 10);
    }

    /**
     * @param values array que contiene los valores que se desean intercambiar.
     * @param i      primera posicion del array que se desea intercambiar.
     * @param j      segunda posicion del array que se desea intercambiar.
     */
    public static void swap(int[] values, int i, int j) {
        final int length = values.length;
        if (i >= length || i < 0 || j >= length || j < 0) {
            throw new ArrayIndexOutOfBoundsException(String.format("El índice es inválido: [%d, %d]. Length: %d", i, j, length));
        }
        values[i] = values[i] + values[j];
        values[j] = values[i] - values[j];
        values[i] = values[i] - values[j];
    }

    /**
     * @param values Arreglo con los valores que se desean ordenar
     * @apiNote se recomienda utilizar {@link java.util.Arrays#sort(int[])} ()}, ya que este utiliza el quicksort de doble pivot.
     */
    public static void sort(int[] values) {
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values.length; j++) {
                if (values[i] < values[j]) {
                    swap(values, i, j);
                }
            }
        }
    }

}
