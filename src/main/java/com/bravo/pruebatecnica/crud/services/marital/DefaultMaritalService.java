package com.bravo.pruebatecnica.crud.services.marital;

import com.bravo.pruebatecnica.crud.entities.MaritalStatus;
import com.bravo.pruebatecnica.crud.repositories.MaritalStatusRepository;
import com.bravo.pruebatecnica.exceptions.rest.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultMaritalService implements MaritalService {

    private final MaritalStatusRepository statusRepository;

    public DefaultMaritalService(MaritalStatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<MaritalStatus> getAll() {
        return this.statusRepository.findAll();
    }

    @Override
    public MaritalStatus getOne(Long id) {
        return statusRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("El estado civil ingresado, no pudo ser encontrado"));
    }
}
