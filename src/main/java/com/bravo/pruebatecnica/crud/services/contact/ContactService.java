package com.bravo.pruebatecnica.crud.services.contact;

import com.bravo.pruebatecnica.crud.entities.Contact;
import com.bravo.pruebatecnica.crud.models.ContactDto;

import java.util.List;

public interface ContactService {

    /**
     * @return List of all contacts registered
     */
    List<Contact> getAll();

    /**
     * @param id the contact's id
     * @return the contact info
     */
    Contact getOne(Long id);

    /**
     * @param dto model holding contact's info
     * @return the contact that has just been created
     */
    Contact create(ContactDto dto);

    /**
     * @param id the contact's id
     * @param dto model holding contact's info
     * @return the contact that has just been modified
     */
    Contact update(Long id, ContactDto dto);

    /**
     * @param id the contact's id that you want to delete
     */
    void delete(Long id);
}
