package com.bravo.pruebatecnica.crud.services.marital;

import com.bravo.pruebatecnica.crud.entities.MaritalStatus;

import java.util.List;

public interface MaritalService {
    /**
     * @return List of {@link com.bravo.pruebatecnica.crud.entities.MaritalStatus} registered
     */
    List<MaritalStatus> getAll();

    /**
     * @param id the id for requested marital status
     * @return the info of {@link com.bravo.pruebatecnica.crud.entities.MaritalStatus} requested
     */
    MaritalStatus getOne(Long id);

}
