package com.bravo.pruebatecnica.crud.services.contact;

import com.bravo.pruebatecnica.crud.entities.Contact;
import com.bravo.pruebatecnica.crud.entities.MaritalStatus;
import com.bravo.pruebatecnica.crud.models.ContactDto;
import com.bravo.pruebatecnica.crud.repositories.ContactRepository;
import com.bravo.pruebatecnica.crud.services.marital.MaritalService;
import com.bravo.pruebatecnica.exceptions.InvalidParamException;
import com.bravo.pruebatecnica.exceptions.rest.BadRequestException;
import com.bravo.pruebatecnica.exceptions.rest.ContactAlreadyExistsException;
import com.bravo.pruebatecnica.exceptions.rest.NotFoundException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class DefaultContactService implements ContactService {

    private final MaritalService maritalService;
    private final ContactRepository contactRepository;

    public DefaultContactService(ContactRepository contactRepository, MaritalService maritalService) {
        this.contactRepository = contactRepository;
        this.maritalService = maritalService;
    }

    @Override
    public List<Contact> getAll() {
        return this.contactRepository.findAll();
    }

    @Override
    public Contact getOne(Long id) {
        if (Objects.isNull(id)) {
            throw new InvalidParamException("id");
        }
        return this.contactRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("El contacto seleccionado, no pudo ser encontrado."));
    }

    @Override
    @Transactional
    public Contact create(ContactDto dto) {
        if(Objects.isNull(dto)){
            throw new BadRequestException("La información del contacto es requerida.");
        }
        this.validateForCreation(dto);
        Contact contact = dto.asBase();
        final MaritalStatus maritalStatus = this.maritalService.getOne(dto.getMaritalStatusId());
        contact.setMaritalStatus(maritalStatus);
        return this.contactRepository.save(contact);
    }

    @Override
    @Modifying
    @Transactional
    public Contact update(Long id, ContactDto dto) {
        final Contact contact = this.getOne(id);
        dto.setId(id);
        this.validateForUpdate(dto);
        final MaritalStatus maritalStatus = this.maritalService.getOne(dto.getMaritalStatusId());
        contact.setMaritalStatus(maritalStatus);
        contact.setPhone(dto.getPhone());
        contact.setEmail(dto.getEmail());
        contact.setFirstName(dto.getFirstName());
        contact.setLastName(dto.getLastName());
        contact.setBirthDate(dto.getBirthDate());
        return this.contactRepository.save(contact);
    }

    @Override
    @Modifying
    @Transactional
    public void delete(Long id) {
        final Contact contact = this.getOne(id);
        this.contactRepository.delete(contact);
    }

    private void validateForCreation(ContactDto dto) {
        final boolean byEmail = this.contactRepository.existsByEmail(dto.getEmail());
        if (byEmail) {
            throw new ContactAlreadyExistsException("Existe un contacto con el correo ingresado.");
        }
        final boolean byPhone = this.contactRepository.existsByPhone(dto.getPhone());
        if (byPhone) {
            throw new ContactAlreadyExistsException("Existe un contacto con el teléfono ingresado.");
        }
    }

    private void validateForUpdate(ContactDto dto) {
        this.contactRepository.findByEmail(dto.getEmail())
                .ifPresent((fromDb) -> {
                    if (!fromDb.getId().equals(dto.getId())) {
                        throw new ContactAlreadyExistsException("Existe otro contacto con el correo ingresado.");
                    }
                });
        this.contactRepository.findByPhone(dto.getPhone())
                .ifPresent((fromDb) -> {
                    if (!fromDb.getId().equals(dto.getId())) {
                        throw new ContactAlreadyExistsException("Existe un contacto con el teléfono ingresado.");
                    }
                });
    }
}
