package com.bravo.pruebatecnica.crud.resources;

import com.bravo.pruebatecnica.exceptions.rest.RestResponseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {RestResponseException.class})
    public ResponseEntity<ApiMessage> handler(RestResponseException e) {
        final ApiMessage message = new ApiMessage(e.getMessage(), e.getStatus());
        return ResponseEntity.status(e.getStatus()).body(message);
    }

    public static class ApiMessage {
        private String message;
        private HttpStatus error;

        private ApiMessage(String message, HttpStatus error) {
            this.message = message;
            this.error = error;
        }

        public String getMessage() {
            return message;
        }

        public HttpStatus getError() {
            return error;
        }
    }
}