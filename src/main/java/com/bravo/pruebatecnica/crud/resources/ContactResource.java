package com.bravo.pruebatecnica.crud.resources;

import com.bravo.pruebatecnica.crud.entities.Contact;
import com.bravo.pruebatecnica.crud.models.ContactDto;
import com.bravo.pruebatecnica.crud.services.contact.ContactService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/contacts")
public class ContactResource {

    private final ContactService contactService;

    public ContactResource(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping
    public ResponseEntity<List<ContactDto>> getAll() {
        final List<ContactDto> contacts = this.contactService.getAll()
                .stream().map(ContactDto::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(contacts);
    }

    @PostMapping
    public ResponseEntity<ContactDto> create(@Valid @RequestBody ContactDto dto) {
        final Contact contact = this.contactService.create(dto);
        return ResponseEntity.ok(new ContactDto(contact));
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<ContactDto> getOne(@PathVariable Long id) {
        final Contact contact = this.contactService.getOne(id);
        return ResponseEntity.ok(new ContactDto(contact));
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<ContactDto> update(@PathVariable Long id, @Valid @RequestBody ContactDto dto) {
        final Contact contact = this.contactService.update(id, dto);
        return ResponseEntity.ok(new ContactDto(contact));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        this.contactService.delete(id);
        return ResponseEntity.status(200).build();
    }
}

