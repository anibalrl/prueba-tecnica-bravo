package com.bravo.pruebatecnica.crud.resources;

import com.bravo.pruebatecnica.crud.models.MaritalStatusDto;
import com.bravo.pruebatecnica.crud.services.marital.MaritalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/marital-status")
public class MaritalResource {

    private final MaritalService maritalService;

    public MaritalResource(MaritalService maritalService) {
        this.maritalService = maritalService;
    }

    @GetMapping
    public ResponseEntity<List<MaritalStatusDto>> getAll() {
        final List<MaritalStatusDto> contacts = this.maritalService.getAll()
                .stream().map(MaritalStatusDto::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(contacts);
    }
}

