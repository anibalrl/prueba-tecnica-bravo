package com.bravo.pruebatecnica.crud.entities;

import javax.persistence.*;

@Entity
@Table(name = "MARITAL_STATUS")
public class MaritalStatus {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DESCRIPTION", length = 12)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}