package com.bravo.pruebatecnica.crud.models;

import com.bravo.pruebatecnica.crud.entities.Contact;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class ContactDto {
    private Long id;
    @NotEmpty(message = "Nombre es requerido")
    private String firstName;
    @NotEmpty(message = "Apellido es requerido")
    private String lastName;
    @NotEmpty(message = "Teléfono es requerido")
    private String phone;
    @NotNull(message = "La fecha de nacimiento es requerida")
    private Date birthDate;
    @NotEmpty(message = "Teléfono es requerido")
    private String email;
    @NotNull(message = "El estado civil es requerido")
    private MaritalStatusDto maritalStatus;

    public ContactDto() {
    }

    public ContactDto(Contact contact) {
        this.id = contact.getId();
        this.firstName = contact.getFirstName();
        this.lastName = contact.getLastName();
        this.phone = contact.getPhone();
        this.birthDate = contact.getBirthDate();
        this.email = contact.getEmail();
        this.maritalStatus = new MaritalStatusDto(contact.getMaritalStatus());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MaritalStatusDto getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatusDto maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Contact asBase() {
        final Contact contact = new Contact();
        contact.setId(id);
        contact.setFirstName(firstName);
        contact.setLastName(lastName);
        contact.setBirthDate(birthDate);
        contact.setEmail(email);
        contact.setPhone(phone);
        return contact;
    }

    @JsonIgnore
    public Long getMaritalStatusId() {
        return maritalStatus.getId();
    }
}
