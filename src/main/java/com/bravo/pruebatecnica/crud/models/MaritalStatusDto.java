package com.bravo.pruebatecnica.crud.models;

import com.bravo.pruebatecnica.crud.entities.MaritalStatus;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class MaritalStatusDto {

    @NotNull(message = "El ID del estado civil es requerido")
    private Long id;
    @NotEmpty(message = "La descripción del estado civil es requerida")
    private String description;

    public MaritalStatusDto() {
    }

    public MaritalStatusDto(MaritalStatus status) {
        this.id = status.getId();
        this.description = status.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
