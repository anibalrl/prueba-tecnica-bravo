package com.bravo.pruebatecnica.crud.repositories;

import com.bravo.pruebatecnica.crud.entities.MaritalStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaritalStatusRepository extends JpaRepository<MaritalStatus, Long> {
}
