package com.bravo.pruebatecnica.crud.repositories;

import com.bravo.pruebatecnica.crud.entities.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ContactRepository extends JpaRepository<Contact, Long> {
    boolean existsByEmail(String email);

    Optional<Contact> findByEmail(String email);

    boolean existsByPhone(String phone);

    Optional<Contact> findByPhone(String phone);
}
