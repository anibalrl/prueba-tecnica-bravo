package com.bravo.pruebatecnica.algorithms;

import com.bravo.pruebatecnica.exceptions.InvalidParamException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class NumbersMultiplyTest {

    @Test
    public void multiply_with_negative_multiplying() {
        InvalidParamException thrown = Assertions.assertThrows(InvalidParamException.class,
                () -> Numbers.multiply(-1, 10)
        );
        Assertions.assertTrue(thrown.getMessage().contains("(multiplying)"));
    }

    @Test
    public void multiply_with_negative_multiplier() {
        InvalidParamException thrown = Assertions.assertThrows(InvalidParamException.class,
                () -> Numbers.multiply(1, -10)
        );
        Assertions.assertTrue(thrown.getMessage().contains("(multiplier)"));
    }

    @Test
    public void multiply_with_zero_multiplying() {
        Assertions.assertEquals(0, Numbers.multiply(0, 10));
    }

    @Test
    public void multiply_with_zero_multiplier() {
        Assertions.assertEquals(0, Numbers.multiply(21, 0));
    }
}