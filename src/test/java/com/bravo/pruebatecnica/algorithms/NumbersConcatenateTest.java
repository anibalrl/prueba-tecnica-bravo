package com.bravo.pruebatecnica.algorithms;

import com.bravo.pruebatecnica.exceptions.InvalidParamException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class NumbersConcatenateTest {

    @Test
    public void concatenate_with_negative_first_param() {
        InvalidParamException thrown = Assertions.assertThrows(InvalidParamException.class,
                () -> Numbers.concatenate(-1, 10)
        );
        Assertions.assertTrue(thrown.getMessage().contains("(a)"));
    }

    @Test
    public void concatenate_with_negative_second_param() {
        InvalidParamException thrown = Assertions.assertThrows(InvalidParamException.class,
                () -> Numbers.concatenate(1, -10)
        );
        Assertions.assertTrue(thrown.getMessage().contains("(b)"));
    }

    @Test
    public void concatenate_with_zero_values() {
        int concatenate = Numbers.concatenate(0, 0);
        Assertions.assertEquals(0, concatenate);
    }

    @Test
    public void concatenate_with_real_values() {
        int concatenate = Numbers.concatenate(10, 25);
        Assertions.assertEquals(1025, concatenate);
    }

    @Test
    public void concatenate_with_given_value_first() {
        int concatenate = Numbers.concatenate(1, 3);
        Assertions.assertEquals(13, concatenate);
    }

    @Test
    public void concatenate_with_given_value_second() {
        int concatenate = Numbers.concatenate(2, 0);
        Assertions.assertEquals(20, concatenate);
    }

    @Test
    public void concatenate_with_given_value_third() {
        int concatenate = Numbers.concatenate(0, 2);
        Assertions.assertEquals(2, concatenate);
    }
}
