package com.bravo.pruebatecnica.algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class NumbersSwapTest {

    @Test
    public void swap_with_negative_first_index() {
        final int[] values = {10, 21};
        ArrayIndexOutOfBoundsException thrown = Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,
                () -> Numbers.swap(values, -1, 1)
        );
        Assertions.assertTrue(thrown.getMessage().contains("índice es inválido"));
    }

    @Test
    public void swap_with_large_first_index() {
        final int[] values = {10, 21};
        ArrayIndexOutOfBoundsException thrown = Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,
                () -> Numbers.swap(values, 8524, 0)
        );
        Assertions.assertTrue(thrown.getMessage().contains("índice es inválido"));
    }

    @Test
    public void swap_with_negative_second_index() {
        final int[] values = {10, 21};
        ArrayIndexOutOfBoundsException thrown = Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,
                () -> Numbers.swap(values, 1, -10)
        );
        Assertions.assertTrue(thrown.getMessage().contains("índice es inválido"));
    }

    @Test
    public void swap_with_large_second_index() {
        final int[] values = {10, 21};
        ArrayIndexOutOfBoundsException thrown = Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,
                () -> Numbers.swap(values, 1, 10)
        );
        Assertions.assertTrue(thrown.getMessage().contains("índice es inválido"));
    }

    @Test
    public void swap_with_right_info() {
        int[] values = {10, 21};
        Numbers.swap(values, 0, 1);
        Assertions.assertEquals(10, values[1]);
        Assertions.assertEquals(21, values[0]);
    }
}