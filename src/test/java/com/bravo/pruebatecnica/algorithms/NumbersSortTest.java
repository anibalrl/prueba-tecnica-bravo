package com.bravo.pruebatecnica.algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class NumbersSortTest {

    @Test
    public void sort_with_empty_array() {
        int[] empty = {};
        Numbers.sort(empty);
        Assertions.assertEquals(0, empty.length);
    }

    @Test
    public void sort_with_ordered_input() {
        int[] values = {0, 1, 2, 8};
        Numbers.sort(values);
        Assertions.assertEquals(0, values[0]);
        Assertions.assertEquals(1, values[1]);
        Assertions.assertEquals(2, values[2]);
        Assertions.assertEquals(8, values[3]);
    }

    @Test
    public void sort_with_unordered_input() {
        int[] values = {14, -7, 2, 5};
        Numbers.sort(values);
        Assertions.assertEquals(-7, values[0]);
        Assertions.assertEquals(2, values[1]);
        Assertions.assertEquals(5, values[2]);
        Assertions.assertEquals(14, values[3]);
    }

}