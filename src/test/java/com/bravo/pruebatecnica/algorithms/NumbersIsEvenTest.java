package com.bravo.pruebatecnica.algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class NumbersIsEvenTest {

    @Test
    public void check_if_is_even_with_odd_negative_value() {
        Assertions.assertFalse(Numbers.isEven(-11));
    }

    @Test
    public void check_if_is_even_with_odd_positive_value() {
        Assertions.assertFalse(Numbers.isEven(23));
    }

    @Test
    public void check_if_is_even_with_even_negative_value() {
        Assertions.assertFalse(Numbers.isEven(-1236429));
    }

    @Test
    public void check_if_is_even_with_even_positive_value() {
        Assertions.assertTrue(Numbers.isEven(440));
    }

    @Test
    public void check_if_is_even_with_zero() {
        Assertions.assertTrue(Numbers.isEven(0));
    }
}