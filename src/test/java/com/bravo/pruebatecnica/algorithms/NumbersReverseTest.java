package com.bravo.pruebatecnica.algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class NumbersReverseTest {

    @Test
    public void check_reverse_with_zero() {
        Assertions.assertEquals(0, Numbers.reverse(0));
    }

    @Test
    public void check_reverse_with_valid_number() {
        Assertions.assertEquals(112, Numbers.reverse(211));
    }

    @Test
    public void check_reverse_with_leading_zero() {
        Assertions.assertEquals(2001, Numbers.reverse(1002));
    }

    @Test
    public void check_reverse_with_trailing_zero() {
        Assertions.assertEquals(2, Numbers.reverse(200));
    }

    @Test
    public void check_reverse_with_given_value_first() {
        Assertions.assertEquals(123, Numbers.reverse(321));
    }

    @Test
    public void check_reverse_with_given_value_second() {
        Assertions.assertEquals(32, Numbers.reverse(230));
    }
}