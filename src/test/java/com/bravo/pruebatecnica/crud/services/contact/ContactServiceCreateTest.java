package com.bravo.pruebatecnica.crud.services.contact;

import com.bravo.pruebatecnica.crud.entities.Contact;
import com.bravo.pruebatecnica.crud.entities.MaritalStatus;
import com.bravo.pruebatecnica.crud.models.ContactDto;
import com.bravo.pruebatecnica.crud.repositories.ContactRepository;
import com.bravo.pruebatecnica.crud.services.marital.MaritalService;
import com.bravo.pruebatecnica.exceptions.rest.BadRequestException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public class ContactServiceCreateTest {

    @Test
    public void create_with_empty_body() {
        final ContactRepository contactRepository = Mockito.mock(ContactRepository.class);
        final MaritalService maritalService = Mockito.mock(MaritalService.class);
        final DefaultContactService contactService = new DefaultContactService(contactRepository, maritalService);
        BadRequestException thrown = Assertions.assertThrows(BadRequestException.class,
                () -> contactService.create(null)
        );
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, thrown.getStatus());
    }

    @Test
    public void create_with_existing_phone() {
        final ContactRepository contactRepository = Mockito.mock(ContactRepository.class);
        String phone = "8099803135";
        final long id = 10L;
        final Contact contact = getContact(id);
        contact.setPhone(phone);
        Mockito.when(contactRepository.findByPhone(phone)).thenReturn(Optional.of(contact));
        final MaritalService maritalService = Mockito.mock(MaritalService.class);
        Mockito.when(maritalService.getOne(id)).thenReturn(getMaritalStatus(id));
    }

    Contact getContact(Long id) {
        final Contact contact = new Contact();
        final MaritalStatus status = getMaritalStatus(id);
        contact.setId(id);
        contact.setMaritalStatus(status);
        return contact;
    }

    private MaritalStatus getMaritalStatus(Long id) {
        final MaritalStatus status = new MaritalStatus();
        status.setId(id);
        status.setDescription("Description");
        return status;
    }
}
