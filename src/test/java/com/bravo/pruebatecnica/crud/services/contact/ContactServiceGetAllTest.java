package com.bravo.pruebatecnica.crud.services.contact;

import com.bravo.pruebatecnica.crud.entities.Contact;
import com.bravo.pruebatecnica.crud.repositories.ContactRepository;
import com.bravo.pruebatecnica.crud.services.marital.MaritalService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class ContactServiceGetAllTest {

    @Test
    public void get_all_with_empty_response() {
        final ContactRepository contactRepository = Mockito.mock(ContactRepository.class);
        Mockito.when(contactRepository.findAll()).thenReturn(new ArrayList<>());
        final DefaultContactService contactService = new DefaultContactService(contactRepository, null);
        final List<Contact> contactList = contactService.getAll();
        Assertions.assertEquals(0, contactList.size());
    }

    @Test
    public void get_all_with_multiple_response() {
        final ContactRepository contactRepository = Mockito.mock(ContactRepository.class);
        Mockito.when(contactRepository.findAll()).thenReturn(getContacts());
        final DefaultContactService contactService = new DefaultContactService(contactRepository, null);
        final List<Contact> contactList = contactService.getAll();
        Assertions.assertEquals(2, contactList.size());
    }

    List<Contact> getContacts(){
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact());
        contacts.add(new Contact());
        return contacts;
    }
}
