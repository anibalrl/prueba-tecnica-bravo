package com.bravo.pruebatecnica.crud.services.contact;

import com.bravo.pruebatecnica.crud.entities.Contact;
import com.bravo.pruebatecnica.crud.repositories.ContactRepository;
import com.bravo.pruebatecnica.exceptions.InvalidParamException;
import com.bravo.pruebatecnica.exceptions.rest.NotFoundException;
import com.bravo.pruebatecnica.exceptions.rest.RestResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public class ContactServiceGetOneTest {

    @Test
    public void get_one_with_null_id() {
        final ContactRepository contactRepository = Mockito.mock(ContactRepository.class);
        final DefaultContactService contactService = new DefaultContactService(contactRepository, null);
        InvalidParamException thrown = Assertions.assertThrows(InvalidParamException.class,
                () -> contactService.getOne(null)
        );
        Assertions.assertTrue(thrown.getMessage().contains("id"));
    }

    @Test
    public void get_one_with_not_existing_id() {
        Long id = 10L;
        final ContactRepository contactRepository = Mockito.mock(ContactRepository.class);
        Mockito.when(contactRepository.findById(id)).thenReturn(Optional.empty());
        final DefaultContactService contactService = new DefaultContactService(contactRepository, null);
        RestResponseException thrown = Assertions.assertThrows(NotFoundException.class,
                () -> contactService.getOne(id)
        );
        Assertions.assertEquals(thrown.getStatus(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void get_one_with_existing_id() {
        Long id = 10L;
        final ContactRepository contactRepository = Mockito.mock(ContactRepository.class);
        Mockito.when(contactRepository.findById(id)).thenReturn(getContact(id));
        final DefaultContactService contactService = new DefaultContactService(contactRepository, null);
        final Contact one = contactService.getOne(id);
        Assertions.assertEquals(id, one.getId());
    }

    Optional<Contact> getContact(Long id) {
        final Contact contact = new Contact();
        contact.setId(id);
        return Optional.of(contact);
    }
}
