
# README!

> A continuacion se muestra como ejecutar el proyecto de pueba técnica, realizado en Spring Boot version 2.5.0, por Jose Anibal Rodriguez.

## Instalación

> **Nota:** es necesario tener instalado **`git 2.25.1, Java 1.8 y Maven 3.5, angular cli v12 (Opcional)`**.
##### Paso 1
Clona el repositorio  con el siguiente comando **`git clone https://anibalrl@bitbucket.org/anibalrl/prueba-tecnica-bravo.git`** y muevete a la carpeta desgarda con **`>cd prueba-tecnica-bravo`**.
##### Paso 2
Ejecutar el comando **mvn install** para instalar las dependencias del proyecto maven.
##### Paso 3
Ejecutar el comando **mvn test** para correr las pruebas unitarias.
##### Paso 4
Ejecutar el comando **mvn spring-boot:run** para correr el proyecto.
##### Paso 5
Puede acceder a la url **http://localhost:8080**,  donde estará corriendo la aplicación.

## Contenido del web service.

|   Name  |  Params   | Response |     Method      |         Resource     |
|-------|--------|:--------:|----------|:------:|-------------------|
| Lista de contactos | ` no requiere` | `List<ContactDto>`  |`GET`  |  `/api/v1/contacts`|
|Leer un contacto |`Path(id)` | `ContactDto`  |`GET`  |  `/api/v1/contacts/{id}`|
|Crear contacto|`Json(ContactDto)` | `ContactDto`  |`POST`  |  `/api/v1/contacts`|
|Editar contacto|`Json(ContactDto), Path(id)` | `ContactDto`  |`PUT`  |  `/api/v1/contacts/{id}`|
|Eliminar contacto |`Path(id)` | `void`  |`DELETE`  | `/api/v1/contacts/{id}`|

#### Lista de contactos
```javascript  
curl --location --request GET 'http://localhost:8080/api/v1/contacts'
```  
>**Nota:** No es necesario parametros en este momento.

##### Respuesta
```json  
[
    {
		"id": 10001,
		"firstName": "JOSE ANIBAL",
		"lastName": "RODRIGUEZ LOPEZ",
		"phone": "8099803135",
		"birthDate": "1994-09-27T04:00:00.000+00:00",
		"email": "joseanibal.rodpez@gmail.com",
		"maritalStatus": {
			"id": 1001,
			"description": "Soltero/a"
		}
	}
] 
```  
#### Leer informacion de contacto
```javascript  
curl --location --request GET 'http://localhost:8080/api/v1/contacts/10001'
```  
##### Respuesta
```json  
{
	"id": 10001,
	"firstName": "JOSE ANIBAL",
	"lastName": "RODRIGUEZ LOPEZ",
	"phone": "8099803135",
	"birthDate": "1994-09-27T04:00:00.000+00:00",
	"email": "joseanibal.rodpez@gmail.com",
	"maritalStatus": {
		"id": 1001,
		"description": "Soltero/a"
	}
}  
```    
#### Crear contacto
```javascript  
curl --location --request POST 'http://localhost:8080/api/v1/contacts' \
--header 'Content-Type: application/json' \
--data-raw '{
	"firstName": "Manuel",
	"lastName": "Castrellon",
	"phone": "8099803133",
	"birthDate": "1994-09-27T04:00:00.000+00:00",
	"email": "manuel.rodpez@gmail.com",
	"maritalStatus": {
		"id": 1001,
		"description": "Soltero/a"
	}
}'
```  

##### Respuesta

```json  
{
    "id": 10002,
    "firstName": "Manuel",
    "lastName": "Castrellon",
    "phone": "8099803133",
    "birthDate": "1994-09-27T04:00:00.000+00:00",
    "email": "manuel.rodpez@gmail.com",
    "maritalStatus": {
        "id": 1001,
        "description": "Soltero/a"
    }
}
```  

#### Modificar contacto
```javascript  
curl --location --request PUT 'http://localhost:8080/api/v1/contacts/10002' \
--header 'Content-Type: application/json' \
--data-raw '{
	"firstName": "Manuel",
	"lastName": "Castrellon",
	"phone": "8099803133",
	"birthDate": "1994-09-27T04:00:00.000+00:00",
	"email": "manuel.rodpez@gmail.com",
	"maritalStatus": {
		"id": 1001,
		"description": "Soltero/a"
	}
}'
```  
>**Nota:** el parametro **id**, hace referencia al ID del usuario que se quiere consultar, en caso de no existir, responderá con un 404.

##### Respuesta
 ```json  
{
    "id": 10002,
    "firstName": "Manuel",
    "lastName": "Castrellon",
    "phone": "8099803133",
    "birthDate": "1994-09-27T04:00:00.000+00:00",
    "email": "manuel.rodpez@gmail.com",
    "maritalStatus": {
        "id": 1001,
        "description": "Soltero/a"
    }
}
```  

#### Eliminar contacto
```javascript  
curl --location --request DELETE 'http://localhost:8080/api/v1/contacts/10002'
```  
>**Nota:** Para el caso del borrado de contacto, la respuesta es un HttpStatus 200 en caso de ser exitoso, en caso de no, la respuesta seria uno de los errores manejados.
##### Respuesta

## Errores
> Los errores están compuesto de un **error**, que indica el HttpStatus de la respuesta del error. Entre la lista de errores manejados se encuentran los siguientes:

> com.bravo.pruebatecnica.exceptions.InvalidParamException
> com.bravo.pruebatecnica.exceptions.rest.BadRequestException
> com.bravo.pruebatecnica.exceptions.rest.ContactAlreadyExistsException
> com.bravo.pruebatecnica.exceptions.rest.NotFoundException

Las excepciones se serializan de la siguiente manera, para fines de dar un mensaje de error mas intuitivo:
####  ContactAlreadyExistsException
```json  
{
    "message": "Existe un contacto con el correo ingresado.",
    "error": "CONFLICT"
}
```  

####  BadRequestException
```json  
{
    "message": "Ha habido un inconveniente procesando su solicitud.",
    "error": "BAD_REQUEST"
}
```  

####  ContactAlreadyExistsException
```json  
{
    "message": "Existe un contacto con el correo ingresado.",
    "error": "NOT_FOUND"
}
```  
> Resultado de intentar crear un cliente sin enviar algunos de los campos obligatorios.

## Estructura de datos

##### Estado Civil
``` sql
CREATE TABLE MARITAL_STATUS  
(  
    ID INT AUTO_INCREMENT PRIMARY KEY,  
	DESCRIPTION VARCHAR(64) NOT NULL  
);

INSERT INTO MARITAL_STATUS (ID, DESCRIPTION)  
VALUES (1001, 'Soltero/a'),  
  (1002, 'Casado/a'),  
  (1003, 'Union Libre'),  
  (1004, 'Viudo/a'),  
  (1005, 'Divorciado/a'),  
  (1006, 'Separado/a');
  
```
##### Contacto
``` sql
CREATE TABLE CONTACT
(
    ID             INT AUTO_INCREMENT PRIMARY KEY,
    FIRST_NAME     VARCHAR(64)  NOT NULL,
    LAST_NAME      VARCHAR(64)  NOT NULL,
    PHONE          VARCHAR(10)  NOT NULL,
    BIRTH_DATE     DATE         NOT NULL,
    EMAIL          VARCHAR(255) NOT NULL,
    MARITAL_STATUS INT          NOT NULL,
    CONSTRAINT MARITAL_STATUS_FK FOREIGN KEY (MARITAL_STATUS) REFERENCES MARITAL_STATUS (ID),
    UNIQUE KEY PHONE_UNIQUE (PHONE),
    UNIQUE KEY EMAIL_UNIQUE (EMAIL)
);

  
INSERT INTO CONTACT(ID, FIRST_NAME, LAST_NAME, PHONE, BIRTH_DATE, EMAIL, MARITAL_STATUS)  
VALUES (10001, 'JOSE ANIBAL', 'RODRIGUEZ LOPEZ', '8099803135', '1994-09-27', 'joseanibal.rodpez@gmail.com', 1002);

```

####  Modelos
##### Contacto
``` java
public class Contact {
    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    private Date birthDate;
    private String email;
    private MaritalStatus maritalStatus;
}
```
##### Estado Civil
``` java
public class MaritalStatus {
    private Long id;
    private String description;
}
```
## Desarrollador

* [Jose Anibal Rodriguez ](https://www.linkedin.com/in/anibalrljose/)